# GVariant Specification

A markdown version of the GVariant specification originally in PDF format.

Created by @desrt, converted to markdown by @wmanley.

See the spec in [gvariant-specification.md] based on the original PDF [gvariant-specification.pdf].
